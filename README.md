# House Explorer

**House Explorer** is a Python-based text adventure game engine. 

Development for this game is in its early stages. If you have any feedback or
ideas of any kind, please send them to grc5969@rit.edu.

## Environment
House_Explorer Version: 0.3
Python Version: 3.3

## Running
`python3 house-explorer.py`
